#!/bin/bash

echo -e "============================================================="
echo -e "=== Provision Ubuntu 16.04 for WordPress Applications == ===="
echo -e "============================================================="

if [ -z "$1" ] # Is parameter #1 zero length?
then
	echo -e "=== !!! ERROR: Requires at least one argument. Site name has to be specified."
	exit 1
fi

# SETUP VARIABLES

USER_PASS=tester

DB_NAME=$1
DB_USER=$1
DB_PASS=tester

MYSQL_ROOT_PASS=password

SITE_DOMAIN=$1.dev.rhyndo.com
SITE_URL=http://$SITE_DOMAIN
SITE_TITLE=$1

ADMIN_USER=kris
ADMIN_PASS=tester
ADMIN_EMAIL=kris@rhyndo.com

# Check that wp-cli is installed
hash wp &> /dev/null
if [ $? -eq 1 ]; then
	echo >&2 "=== ERROR: wp-cli is required. Check http://wp-cli.org for more information."
	echo -e "\n=== Just execute the following command in your terminal: \n"
	echo -e "    curl https://raw.github.com/wp-cli/wp-cli.github.com/master/installer.sh | bash \n"
	echo -e "    (Make sure to read the instructions) \n"
	echo -e "=== ${MSG_INSTALL_ABORTED} ===================================================== \n"
	exit 1
fi

# Check that the MySQL server us running
UP=$(pgrep mysql | wc -l);
if [ "$UP" -ne 1 ];
then
	echo -e "=== ERROR: MySQL not running!\n"
	echo -e "=== This script needs MySQL running in order to install and configure"
	echo -e "=== the WordPress database. \n"
	echo -e "=== ${MSG_INSTALL_ABORTED} ===================================================== \n"
	exit 1
fi

# TODO: Check that the Database the script is about to create doesn't exist
# if [ -d $MYSQL_DATA_DIR$DB_USER ]
# then
# 	echo -e "=== ERROR: Database '$DB_USER' aready exists! \n"
# 	echo -e "=== ${MSG_INSTALL_ABORTED} ===================================================== \n"
# 	exit 1
# fi

# TODO: Does the new VHost directory already exists?

echo -e "=== Adding New User: $1                                      "

egrep "^$1" /etc/passwd >/dev/null
if [ $? -eq 0 ]; then
	echo "=== !!! ERROR: $1 exists!   "
	exit 1
else
	useradd -m -p $USER_PASS $1 > /dev/null 2>&1
	[ $? -eq 0 ] && echo "=== Adding New User: $1 - Done" || echo "=== !!! ERROR: Failed to add a user!"
fi

echo -e "=== Creating folder structure                            ===="

su $1 -c 'mkdir ~/htdocs'
su $1 -c 'mkdir ~/htdocs/wordpress'
su $1 -c 'mkdir ~/config'
su $1 -c 'mkdir ~/config/nginx'
su $1 -c 'mkdir ~/scripts'
su $1 -c 'mkdir ~/database'

echo -e "=== Download latest WordPress                            ===="

su $1 -c "cd ~/htdocs/wordpress && wp core download | grep 'Success'" > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: WordPress cannot be downloaded            ===="
	exit 1
fi

echo -e "=== Move and rename /wp-content                          ===="

su $1 -c 'mv ~/htdocs/wordpress/wp-content ~/htdocs/content' > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot move /wp-content                   ===="
	exit 1
fi

su $1 -c 'mkdir ~/htdocs/content/uploads && chmod -R 777 ~/htdocs/content/uploads' > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot change /uploads permission         ===="
	exit 1
fi

echo -e "=== Copying and updating WP config files                 ===="

cp boilerplates/apps/wordpress/index.php /home/$1/htdocs/index.php > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot copy index.php                     ===="
	exit 1
fi

cp boilerplates/apps/wordpress/wp-config.php /home/$1/htdocs/wp-config.php > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot copy wp-config.php                 ===="
	exit 1
fi

sed -i.bak "s@{SITE_URL}@$SITE_URL@g; s@{SITE_NAME}@$SITE_TITLE@g; s@{DB_NAME}@$DB_NAME@; s@{DB_USER}@$DB_USER@; s@{DB_PASS}@$DB_PASS@" /home/$1/htdocs/wp-config.php

cp boilerplates/nginx/sites-available/nginx-virtual-host-app-wordpress.conf /etc/nginx/sites-available/$1.conf > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot copy virtual-host-app-wordpress    ===="
	exit 1
fi

sed -i.bak "s@{SITE_TITLE}@$SITE_DOMAIN@; s@{SITE_ROUTE}@/home/$1/htdocs/@" /etc/nginx/sites-available/$1.conf

ln -s /etc/nginx/sites-available/$1.conf /etc/nginx/sites-enabled/$1.conf > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot create symlink                     ===="
	exit 1
fi

chown $1:$1 /home/$1/htdocs/index.php > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot change index.php permission        ===="
	exit 1
fi

chown $1:$1 /home/$1/htdocs/wp-config.php > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot change wp-config.php permission    ===="
	exit 1
fi

echo -e "=== Restarting web server                                ===="

service nginx reload > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: NGINX Cannot reload                       ===="
	exit 1
fi

echo -e "=== Create MySQL Database and User                       ===="

mysql -uroot -p$MYSQL_ROOT_PASS -e "CREATE DATABASE ${DB_USER} /*\!40100 DEFAULT CHARACTER SET utf8 */;" > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot create database                    ===="
	exit 1
fi

mysql -uroot -p$MYSQL_ROOT_PASS -e "CREATE USER ${DB_USER}@localhost IDENTIFIED BY '${DB_PASS}';" > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot create mysql user                  ===="
	exit 1
fi

mysql -uroot -p$MYSQL_ROOT_PASS -e "GRANT ALL PRIVILEGES ON ${DB_USER}.* TO '${DB_USER}'@'localhost';" > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Mysql cannot grant privileges             ===="
	exit 1
fi

mysql -uroot -p$MYSQL_ROOT_PASS -e "FLUSH PRIVILEGES;" > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Mysql cannot flush privileges             ===="
	exit 1
fi

echo -e "=== Installing WordPress                                 ===="

su $1 -c 'cd ~/htdocs/wordpress/ && wp core install --url='$SITE_URL' --title='$SITE_TITLE' --admin_user='$ADMIN_USER' --admin_password='$ADMIN_PASS' --admin_email='$ADMIN_EMAIL > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: WordPress cannot install                  ===="
	exit 1
fi

su $1 -c "cd ~/htdocs/wordpress/ && wp rewrite structure '/%postname%/'" > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: WP cannot rewrite structure               ===="
	exit 1
fi

echo -e "============================================================="
echo -e "=== WordPress installation & configuration - Done        ===="
echo -e "============================================================="
