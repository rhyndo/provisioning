#!/usr/bin/env bash

##
# Script for extracting single site from multisite
##
 
#SETUP
 
#static
wpdb="DATABASE NAME"
dbuser="DATABASE USER"
dbpass="DATABASE PASSWORD"
wpdirectory=/data/wordpress/htdocs/wordpress

if [ -z "$1" ]
  then
    echo "Please provide site ID as argument"
    exit 1
else
    echo "Extracting site with ID: $1"
fi
blogid=$1
#MAIN
if [ ! -d $blogid ]; then
  mkdir $blogid
fi
cd $blogid
 
#db - blog's related tables
echo "creating sql dumps..."
mysql -u $dbuser -p$dbpass -N information_schema -e "select table_name from tables where table_schema = '${wpdb}' and table_name like '%wp\_${blogid}\_%'" > tables.txt
mysqldump -u $dbuser -p$dbpass $wpdb `cat tables.txt` > wordpress.sql
rm tables.txt
sed -i "s,wp_${blogid}_,wp_,g" wordpress.sql
 
 #db - users & usermeta tables
 userids=($(mysql -u $dbuser -p$dbpass $wpdb -Bse "select distinct user_id from wp_usermeta where meta_key like 'wp\_${blogid}\_%'"))
 userids_cnt=${#userids[@]}
 where_users="ID=${userids[0]}"
 where_meta="user_id=${userids[0]}"
 
 for (( i=1; i <${userids_cnt}; i++))
 do
        where_users="$where_users or ID=${userids[$i]}"
        where_meta="$where_meta or user_id=${userids[$i]}"
 done
 
 mysqldump -u $dbuser -p$dbpass $wpdb wp_users --where "${where_users}"  > wp_users.sql
 mysqldump -u $dbuser -p$dbpass $wpdb wp_usermeta --where "${where_meta}"  > wp_usermeta.sql
 sed -i "s,wp_${blogid}_,wp_,g" wp_usermeta.sql
 #plugins
echo "copying plugins...."
 plugins=($(mysql -u $dbuser -p$dbpass $wpdb -Bse "select * from wp_${blogid}_options where option_name='active_plugins'" | grep -Po ':"\K[^/]*'))
 plugins_cnt=${#plugins[@]}
 if [ ! -d plugins ]; then
  mkdir plugins
 fi
 for (( p=0; p <${plugins_cnt}; p++))
 do
    if [[ ${plugins[p]} = *hello.php* ]]; then
        hello=($(echo ${plugins[p]} | awk -F\" '{print $NF}'))
        cp -r $wpdirectory/wp-content/plugins/${hello} ./plugins/
    else
        cp -r $wpdirectory/wp-content/plugins/${plugins[p]} ./plugins/
    fi
 done

wp_theme=($(mysql -u $dbuser -p$dbpass $wpdb -Bse "select option_value from wp_${blogid}_options where option_name='template'"))
if [ ! -d themes ]; then
    mkdir themes
fi
echo "copying theme $wp_theme...."
cp -r $wpdirectory/wp-content/themes/$wp_theme ./themes/$wp_theme

wp_parent_theme=($(mysql -u $dbuser -p$dbpass $wpdb -Bse "select option_value from wp_${blogid}_options where option_name='template'"))
if [ "$wp_theme" != "$wp_parent_theme" ]; then
    echo "copying parent theme $wp_parent_theme too...."
    cp -r $wpdirectory/wp-content/themes/$wp_parent_theme ./themes/$wp_parent_theme
fi

if [ -d $wpdirectory/wp-content/mu-plugins ]; then
    echo "copying mu-plugins...."
    cp -r $wpdirectory/wp-content/mu-plugins/ ./mu-plugins/
fi

#uploads
echo "copying uploads folder...."
if [ ! -d uploads ]; then
    mkdir uploads
fi

# If this is really old multisite uploads used to be in wp-content/blogs.dir
if [ -d $wpdirectory/wp-content/blogs.dir/${blogid}/files ]; then
    cp -r $wpdirectory/wp-content/blogs.dir/${blogid}/files/* ./uploads/
else
    # If its newer site copy the files from where they should be
    cp -r $wpdirectory/wp-content/uploads/sites/${blogid}/ ./uploads/
fi
echo "Your files are in $(pwd)" 
echo "Done!"