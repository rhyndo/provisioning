#!/bin/bash

echo -e "============================================================="
echo -e "=== Provision Ubuntu 16.04 for WordPress Applications == ===="
echo -e "============================================================="

if [ -z "$1" ] # Is parameter #1 zero length?
then
	echo -e "=== !!! ERROR: Requires at least one argument. Site name has to be specified."
	exit 1
fi

# SETUP VARIABLES

USER_PASS=tester

DB_NAME=$1
DB_USER=$1
DB_PASS=tester

MYSQL_ROOT_PASS=password

SITE_DOMAIN=$1.com

SITE_URL=http://$SITE_DOMAIN
SITE_TITLE=$1

ADMIN_USER=kris
ADMIN_PASS=tester
ADMIN_EMAIL=kris@rhyndo.com


# Check that wp-cli is installed
hash wp &> /dev/null
if [ $? -eq 1 ]; then
	echo >&2 "=== ERROR: wp-cli is required. Check http://wp-cli.org for more information."
	echo -e "\n=== Just execute the following command in your terminal: \n"
	echo -e "    curl https://raw.github.com/wp-cli/wp-cli.github.com/master/installer.sh | bash \n"
	echo -e "    (Make sure to read the instructions) \n"
	echo -e "=== ${MSG_INSTALL_ABORTED} ===================================================== \n"
	exit 1
fi

# Check that the MySQL server us running
UP=$(pgrep mysql | wc -l);
if [ "$UP" -ne 1 ];
then
	echo -e "=== ERROR: MySQL not running!\n"
	echo -e "=== This script needs MySQL running in order to install and configure"
	echo -e "=== the WordPress database. \n"
	echo -e "=== ${MSG_INSTALL_ABORTED} ===================================================== \n"
	exit 1
fi

# TODO: Check that the Database the script is about to create doesn't exist
# if [ -d $MYSQL_DATA_DIR$DB_USER ]
# then
# 	echo -e "=== ERROR: Database '$DB_USER' aready exists! \n"
# 	echo -e "=== ${MSG_INSTALL_ABORTED} ===================================================== \n"
# 	exit 1
# fi

# TODO: Does the new VHost directory already exists?

echo -e "=== Adding New User: $1                                      "

egrep "^$1" /etc/passwd >/dev/null
if [ $? -eq 0 ]; then
	echo "=== !!! ERROR: $1 exists!   "
	exit 1
else
	useradd -m -p $USER_PASS $1 > /dev/null 2>&1
	[ $? -eq 0 ] && echo "=== Adding New User: $1 - Done" || echo "=== !!! ERROR: Failed to add a user!"
fi

echo -e "=== Creating folder structure                            ===="

su $1 -c 'mkdir ~/htdocs'

echo -e "=== Copying and updating WP config files                 ===="

cp boilerplates/apps/wordpress/index.php /home/$1/htdocs/index.php > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot copy index.php                     ===="
	exit 1
fi

cp boilerplates/apps/wordpress/wp-network-config.php /home/$1/htdocs/wp-config.php > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot copy wp-network-config.php         ===="
	exit 1
fi

sed -i.bak "s@{SITE_DOMAIN}@$SITE_DOMAIN@; s@{DB_NAME}@$DB_NAME@; s@{DB_USER}@$DB_USER@; s@{DB_PASS}@$DB_PASS@" /home/$1/htdocs/wp-config.php

rm /etc/nginx/sites-enabled/default > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot remove default nginx config        ===="
	exit 1
fi

cp boilerplates/nginx/sites-available/nginx-virtual-host-app-wp-network.conf /etc/nginx/sites-available/$1.conf > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot copy virtual-host-app-wp-network   ===="
	exit 1
fi

sed -i.bak "s/{SITE_ROUTE}/$1/; s/{SITE_TITLE}/$SITE_DOMAIN/" /etc/nginx/sites-available/$1.conf

ln -s /etc/nginx/sites-available/$1.conf /etc/nginx/sites-enabled/$1.conf > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot create symlink                     ===="
	exit 1
fi

chown $1:$1 /home/$1/htdocs/index.php > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot change index.php permission        ===="
	exit 1
fi

chown $1:$1 /home/$1/htdocs/wp-config.php > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot change wp-config.php permission    ===="
	exit 1
fi

echo -e "=== Restarting web server                                ===="

service nginx reload > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: NGINX Cannot reload                       ===="
	exit 1
fi

echo -e "=== Download latest WordPress                            ===="

su $1 -c "cd ~/htdocs/ && wp core download | grep 'Success'" > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: WordPress cannot be downloaded            ===="
	exit 1
fi

echo -e "=== Create MySQL Database and User                       ===="

mysql -uroot -p$MYSQL_ROOT_PASS -e "CREATE DATABASE ${DB_USER} /*\!40100 DEFAULT CHARACTER SET utf8 */;" > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot create database                    ===="
	exit 1
fi

mysql -uroot -p$MYSQL_ROOT_PASS -e "CREATE USER ${DB_USER}@localhost IDENTIFIED BY '${DB_PASS}';" > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot create mysql user                  ===="
	exit 1
fi

mysql -uroot -p$MYSQL_ROOT_PASS -e "GRANT ALL PRIVILEGES ON ${DB_USER}.* TO '${DB_USER}'@'localhost';" > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Mysql cannot grant privileges             ===="
	exit 1
fi

mysql -uroot -p$MYSQL_ROOT_PASS -e "FLUSH PRIVILEGES;" > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Mysql cannot flush privileges             ===="
	exit 1
fi

echo -e "=== Installing WordPress                                 ===="

su $1 -c 'cd ~/htdocs/ && wp core multisite-install --subdomains --url='$SITE_URL' --title='$SITE_TITLE' --admin_user='$ADMIN_USER' --admin_password='$ADMIN_PASS' --admin_email='$ADMIN_EMAIL > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: WordPress cannot install                  ===="
	exit 1
fi

echo -e "=== Creating WordPress Sites                             ===="

su $1 -c 'cd ~/htdocs/ && wp site create --slug='first' --title='first' --email='$ADMIN_EMAIL > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: WordPress cannot create first site        ===="
	exit 1
fi

su $1 -c 'cd ~/htdocs/ && wp site create --slug='second' --title='second' --email='$ADMIN_EMAIL > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: WordPress cannot create second site       ===="
	exit 1
fi

echo -e "=== Install wordpress plugins                            ===="

su $1 -c 'cd ~/htdocs/ && wp plugin install disable-comments' > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: WordPress cannot install plugins          ===="
	exit 1
fi

echo -e "=== Install wordpress plugins                            ===="

su $1 -c 'cd ~/htdocs/ && wp plugin activate disable-comments --network' > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: WordPress cannot activate plugins         ===="
	exit 1
fi

echo -e "============================================================="
echo -e "=== WordPress installation & configuration - Done        ===="
echo -e "============================================================="
