#!/bin/bash

if [ -z "$1" ] # Is parameter #1 zero length?
then
	echo -e "=== ERROR: Requires at least one argument. Site name has to be specified."
	exit 1
fi

# wp plugin update-all > /dev/null 2>&1

dev=(
	better-search-replace
	regenerate-thumbnails
	ssh-sftp-updater-support
)

mu=(
	wordpress-seo
)

social=(
	nextgen-facebook
)

store=(
	woocommerce
)

eval CASE=\${$1[@]}

wp plugin install --activate $CASE