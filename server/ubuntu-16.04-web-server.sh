#!/bin/bash

echo -e "============================================================="
echo -e "=== Provision Ubuntu 16.04 for Web Hosting = = = = = = = ===="
echo -e "============================================================="

# Setup variables

SERVER_ADMIN_EMAIL=kris@rhyndo.com

MYSQL_ROOT_USER=root
MYSQL_ROOT_PASS=password

# Make sure we are working with the latest software.
# Update package manager and Upgrade the system

echo -e "=== Updating the system                                  ===="

apt-get update -y > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== Cannot update package manager                        ==="
	exit 1
fi

apt-get upgrade -y > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== Cannot upgrade package manager                       ==="
	exit 1
fi

echo -e "=== Done                                                 ===="
echo -e "============================================================="
echo -e "=== Installing software                                  ===="
echo -e "=== - Various system tools                               ===="

apt-get install -y debconf-utils vim wget git curl ack-grep htop unzip > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Various system tools                      ==="
	exit 1
fi

echo -e "============================================================="
echo -e "=== Starting server configuration                        ===="
echo -e "=== - Host file                                          ===="

sed -i.bak "s/127.0.0.1\tlocalhost/127.0.0.1\tlocalhost localhost.localdomain $HOSTNAME/g" /etc/hosts

echo -e "=== - New user setup (/etc/skel)                         ===="

mkdir /etc/skel/.ssh

cp boilerplates/ssh/authorized_keys /etc/skel/.ssh/ > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: copy authorized_keys                      ==="
	exit 1
fi

cp boilerplates/bash/bash_settings /etc/skel/.bashrc > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: copy bash_settings to /etc/skel/.bashrc   ==="
	# exit 1
fi

# TODO: Check if .ssh folder exists and create it if not
#cat boilerplates/ssh/authorized_keys >> /root/.ssh/authorized_keys

cp boilerplates/bash/bash_settings /root/.bashrc

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: copy bash_settings to /root/.bashrc       ==="
	# exit 1
fi

echo -e "=== NGINX Install and setup                              ===="

apt-get install -y nginx > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: NGINX cannot install                      ===="
	exit 1
fi

service nginx start > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: NGINX cannot start                        ===="
	exit 1
fi

sed -i.bak "s/# server_tokens off;/server_tokens off;/g" /etc/nginx/nginx.conf

cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default.backup > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: nginx default backup                      ===="
	# exit 1
fi

rm /etc/nginx/sites-available/default > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: nginx remove default /sites-available     ===="
	exit 1
fi

rm /etc/nginx/sites-enabled/default > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: nginx remove default /sites-enabled       ===="
	exit 1
fi

cp boilerplates/nginx/sites-available/virtual-host-default.conf /etc/nginx/sites-available/default > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: copy virtual-host-default.conf to /default===="
	exit 1
fi

ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: symlink /sites-available to /sites-enabled===="
	exit 1
fi

cp boilerplates/nginx/snippets/nginx-virtual-host-snippet-gzip.conf /etc/nginx/snippets/ > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: copy nginx-virtual-host-snippet-gzip.conf ===="
	exit 1
fi

cp boilerplates/nginx/snippets/nginx-virtual-host-snippet-wordpress.conf /etc/nginx/snippets/ > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: copy nginx-virtual-host-snippet-wordpress ===="
	exit 1
fi

rm /var/www/html/index.nginx-debian.html > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: remove index.nginx-debian.html            ===="
	exit 1
fi

cp boilerplates/serverpage/index.html /var/www/html/ > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: copy index.html to /var/www/html          ===="
	# exit 1
fi

service nginx restart > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: NGINX cannot restart                      ===="
	exit 1
fi

echo -e "=== NGINX Done                                           ===="
echo -e "============================================================="
echo -e "=== MYSQL                                                ===="

debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQL_ROOT_PASS" > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: mysql debconf-set-selections              ===="
	exit 1
fi

debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQL_ROOT_PASS" > /dev/null 2>&1

apt-get install -y mysql-server > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot install mysql server               ===="
	exit 1
fi

echo -e "============================================================="
echo -e "=== PHP Stack and modules                                ===="

apt-get install -y php7.0-curl php7.0-gd php7.0-xml php7.0-xmlrpc php7.0 php7.0-fpm php7.0-cli php7.0-mcrypt php7.0-mbstring php7.0-mysql > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot install PHP stack                  ===="
	exit 1
fi

sed -i.bak "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g" /etc/php/7.0/fpm/php.ini

service php7.0-fpm restart > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: PHP-FPM Cannot restart                    ===="
	exit 1
fi

# echo -e "============================================================="
# echo -e "*** Python PIP - - - \n"

# apt-get install -y python-pip > /dev/null 2>&1

# pip install speedtest-cli > /dev/null 2>&1
# pip install --upgrade pip > /dev/null 2>&1

# TODO fix 
# Traceback (most recent call last):
#   File "/usr/bin/pip", line 11, in <module>
#     sys.exit(main())
#   File "/usr/lib/python2.7/dist-packages/pip/__init__.py", line 215, in main
#     locale.setlocale(locale.LC_ALL, '')
#   File "/usr/lib/python2.7/locale.py", line 581, in setlocale
#     return _setlocale(category, locale)
# locale.Error: unsupported locale setting

#if [ $? != 0 ]
#then
#	echo -e "=== Cannot complete install Pip ==="
	# exit 1
#fi

echo -e "============================================================="
echo -e "=== Sendmail                                             ===="

apt-get install -y sendmail > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot install sendmail                   ===="
	# exit 1
fi

# TODO: Configure sendmail

echo -e "============================================================="
echo -e "=== WP-CLI                                               ===="

wget https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot download wp-cli                    ===="
	exit 1
fi

chmod +x wp-cli.phar > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot change wp-cli.phar permission      ===="
	exit 1
fi

mv wp-cli.phar /usr/local/bin/wp > /dev/null 2>&1

if [ $? != 0 ]
then
	echo -e "=== !!! ERROR: Cannot move wp-cli.phar                   ===="
	exit 1
fi

echo -e "============================================================="
echo -e "=== Server configuration - Done                          ===="
echo -e "============================================================="
